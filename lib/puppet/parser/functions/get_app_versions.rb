require 'rubygems'
require 'rest-client'
require 'json'
require 'facter'

module Puppet::Parser::Functions
      	newfunction(:get_app_versions, :type => :rvalue) do
		rest = RestClient.get "http://appinfo:2080/api/servers/#{lookupvar('fqdn')}"

		    if(rest == "null")
		        apps = nil
		    else
		        server = JSON.parse(rest)
		        apps = RestClient.get "http://appinfo:2080/api/servers/#{server['id']}/application_versions"
		    end

		    apps
      	end
end
