require 'rubygems'
require 'rest-client'
require 'json'

module Puppet::Parser::Functions
      newfunction(:update_app_versions) do |args|
        url = args[0]
	token = args[1]
        applications_json = args[2]

	applications = JSON.parse(applications_json)

	applications.each do |application|
		update = { :application => application, :id => application['id'] }


		update[:application].delete("updated_at")
		update[:application].delete("id")
		update[:application].delete("user_id")
		update[:application].delete("created_at")

		RestClient::Request.execute(:method => :put, :url => "#{url}/api/applications/#{update[:id]}.json?auth_token=#{token}", :timeout => 10, :open_timeout => 10, :payload => update.to_json)
	end
      end
end
