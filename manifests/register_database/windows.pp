define appinfo::register_database::windows ($database = $title, $dbms, $application){
        $appinfo        = hiera_hash('appinfo')
        $appinfo_token  = $appinfo['token']
        $appinfo_url    = $appinfo['url']

	$application_name_encode	= regsubst($application, '\s', '%20', 'G')

        $server_name		= downcase($::fqdn)
	$onlyif_command		= "if (`c:\appinfo\bin\curl.exe -s '${appinfo_url}/api/databases/${database}.json?auth_token=${appinfo_token}.json'` | select-string -quiet '(\"status\":404)') { exit 0 } else {exit 1}"
        $create_command 	= "c:\appinfo\bin\curl -s -X POST --data \"auth_token=${appinfo_token}&database[name]=${database}&database[dbms]=${dbms}\" ${appinfo_url}/api/applications/${application_name_encode}/databases.json"

        exec { "appinfo_create_database_${server_name}_${database}_${dbms}_${application}":
                command 	=> $create_command,
                onlyif  	=> $onlyif_command,
                path    	=> ['/usr/bin','/bin'],
                require 	=> Appinfo::Register_server[$::fqdn],
                provider	=> powershell,
        }
}
