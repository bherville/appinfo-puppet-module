define appinfo::register_database::linux ($database = $title, $dbms, $application){
        $appinfo        = hiera_hash('appinfo')
        $appinfo_token  = $appinfo['token']
        $appinfo_url    = $appinfo['url']

	$application_name_encode	= regsubst($application, '\s', '%20', 'G')

        $server_name		= downcase($::fqdn)
        $onlyif_command       	= "test `curl -s '${appinfo_url}/api/databases/${database}.json?auth_token=${appinfo_token}.json' | grep -oP '(\"status\":404)'`"
        $create_command		= "curl -s -X POST --data \"auth_token=${appinfo_token}&database[name]=${database}&database[dbms]=${dbms}\" ${appinfo_url}/api/applications/${application_name_encode}/databases.json"

        exec { "appinfo_create_database_${server_name}_${database}_${dbms}_${application}":
                command => $create_command,
                onlyif  => $onlyif_command,
                path    => ['/usr/bin','/bin'],
                require => Appinfo::Register_server[$::fqdn],
                notify  => Exec['update_hosted_applications'],
        }
}
