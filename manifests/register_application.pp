define appinfo::register_application ($application_name = $title, $application_type, $version_command, $major_application = true, $register_server_application = true){
	case $::kernel {
		'Linux': {
			appinfo::register_application::linux { $application_name:
				application_name	=> $application_name,
				application_type	=> $application_type,
				version_command		=> $version_command,
				major_application	=> $major_application,
			}
		}
		'windows':  {
			appinfo::register_application::windows { $application_name:
                                application_name        => $application_name,
                                application_type        => $application_type,
                                version_command         => $version_command,
                                major_application       => $major_application,
                        }
		}
	}

	if ($register_server_application == true) {
		Appinfo::Register_application::Windows {
			before	=> Appinfo::Register_server_application[$application_name],
		}

		Appinfo::Register_application::Linux {
			before	=> Appinfo::Register_server_application[$application_name],
		}

		if ! defined(Appinfo::Register_server_application[$application_name]) {
			appinfo::register_server_application { $application_name: }
		}
	}
}
