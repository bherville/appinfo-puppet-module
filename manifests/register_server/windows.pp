define appinfo::register_server::windows ($server_name = $title) {
	$appinfo 	= hiera_hash('appinfo')
	$appinfo_token 	= $appinfo['token']
	$appinfo_url	= $appinfo['url']

	$downcase_server_name = downcase($server_name)
	$onlyif_command 	= "if (`c:\appinfo\bin\curl.exe -s ${appinfo_url}/api/servers/${downcase_server_name}.json?auth_token=${appinfo_token}.json` | select-string -quiet '(\"status\":404)') { exit 0 } else {exit 1}"
	$create_command 	= "c:\appinfo\bin\curl.exe -s --data \"auth_token=${appinfo_token}&server[name]=${downcase_server_name}&server[active]=true\" ${appinfo_url}/api/servers.json"

	exec { "appinfo_create_server_${downcase_server_name}":
                command 	=> $create_command,
		onlyif  	=> $onlyif_command,
		provider	=> powershell,
        }
}
