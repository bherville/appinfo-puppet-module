define appinfo::register_server::linux ($server_name = $title) {
	$appinfo 	= hiera_hash('appinfo')
	$appinfo_token 	= $appinfo['token']
	$appinfo_url	= $appinfo['url']

	$downcase_server_name 	= downcase($server_name)
	$onlyif_command 	= "test `curl -s ${appinfo_url}/api/servers/${downcase_server_name}.json?auth_token=${appinfo_token}.json | grep -oP '(\"status\":404)'`"
	$create_command 	= "curl -s --data \"auth_token=${appinfo_token}&server[name]=${downcase_server_name}&server[active]=true\" ${appinfo_url}/api/servers.json"

	exec { "appinfo_create_server_${downcase_server_name}":
		command	=> $create_command,
		onlyif	=> $onlyif_command,
		path	=> ['/usr/bin','/bin'],
	}
}
