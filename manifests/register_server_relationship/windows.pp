define appinfo::register_server_relationship::windows ($server_primary = 'nil', $server_secondary, $port, $description = 'nil'){
        $appinfo        = hiera_hash('appinfo')
        $appinfo_token  = $appinfo['token']
        $appinfo_url    = $appinfo['url']

        $server_primary_downcase	= downcase($server_primary)
        $server_secondary_downcase	= downcase($server_secondary)

        $onlyif_command	= "if (`c:\appinfo\bin\curl.exe -s \"${appinfo_url}/api/servers/${server_primary_downcase}/server_relationships/${server_secondary_downcase}.json?auth_token=${appinfo_token}&port=${port}\"` | select-string -quiet '(\"status\":404)') { exit 0 } else {exit 1}"
        $create_command	= "c:\appinfo\bin\curl -s -X POST --data \"auth_token=${appinfo_token}&server_relationship[server_secondary_id]=${server_secondary_downcase}&server_relationship[port]=${port}&server_relationship[description]=${description}\" ${appinfo_url}/api/servers/${server_primary_downcase}/server_relationships.json"

	if ! defined(Exec["appinfo_create_relationships_${server_primary_downcase_downcase}_${server_secondary_downcase_downcase}_${port}"]) {
       		exec { "appinfo_create_relationships_${server_primary_downcase_downcase}_${server_secondary_downcase_downcase}_${port}":
                	command 	=> $create_command,
                	onlyif  	=> $onlyif_command,
			provider	=> powershell,
        	}
	}
}
