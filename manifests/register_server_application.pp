import "register_server_application/*.pp"

define appinfo::register_server_application ($application_name = $title) {
	case $::kernel {
		'Linux': {
			appinfo::register_server_application::linux { $application_name:
				application_name	=> $application_name,
			}
		}
		'windows':  {
			appinfo::register_server_application::windows { $application_name:
                                application_name        => $application_name,
                        }
		}
	}
}
