class appinfo::hosted_applications {
	$appinfo = hiera_hash('appinfo')

	file { '/scripts':
		ensure	=> directory,
		owner   => 'root',
                group   => 'root',
                mode    => '0755',
	}

	file { '/scripts/update_hosted_applications.rb':
		ensure	=> present,
		owner	=> 'root',
		group	=> 'root',
		mode	=> '0750',
		content => template ("util/header.erb",
				     "appinfo/update_hosted_applications.rb.erb"
				    ),
		require	=> File['/scripts'],
	}

	file { '/etc/hosted_applications.yml':
		ensure  => present,
                owner   => 'root',
                group   => 'root',
		mode	=> '0640',
		content => template ("util/header.erb",
                                     "appinfo/hosted_applications.yml.erb"
                                    ),
		require	=> Appinfo::Register_server[$::fqdn],
		notify	=> Exec['update_hosted_applications'],
	}

	cron { 'update_hosted_applications':
  		command => 'ruby /scripts/update_hosted_applications.rb -y /etc/hosted_applications.yml',
  		user    => "root",
		hour	=> '*',
  		minute  => 0,
	}

	exec { 'update_hosted_applications':
		refreshonly	=> true,
		command		=> 'ruby /scripts/update_hosted_applications.rb -y /etc/hosted_applications.yml',
		path		=> '/usr/bin:/bin',
		require 	=> [File['/scripts/update_hosted_applications.rb'], File['/etc/hosted_applications.yml']],
	}
}
