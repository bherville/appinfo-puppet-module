define appinfo::register_application_family_application::linux ($application_family_name, $application_name) {
	$appinfo 	= hiera_hash('appinfo')
	$appinfo_token 	= $appinfo['token']
	$appinfo_url	= $appinfo['url']

	$application_name_encode	= regsubst($application_name, '\s', '%20', 'G')
	$application_family_name_encode	= regsubst($application_family_name, '\s', '%20', 'G')

	$server_name		= downcase($::fqdn)
	$onlyif_command 	= "test `curl -s '${appinfo_url}/api/application_families/${application_family_name_encode}/application_family_applications/${application_name_encode}.json?auth_token=${appinfo_token}' | grep -oP '(\"status\":404)'`"
	$create_command 	= "curl -s -X POST --data \"auth_token=${appinfo_token}&application_family_application[application_id]=${application_name_encode}\" ${appinfo_url}/api/application_families/${application_family_name_encode}/application_family_applications.json"

	exec { "appinfo_register_application_family_application_${server_name}_${application_family_name}_${application_name}":
		command	=> $create_command,
		onlyif	=> $onlyif_command,
                path    => ['/usr/bin','/bin'],
		notify	=> Exec['update_hosted_applications'],
        }
}
