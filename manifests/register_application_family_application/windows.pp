define appinfo::register_application_family_application::windows ($application_family_name, $application_name) {
        $appinfo        = hiera_hash('appinfo')
        $appinfo_token  = $appinfo['token']
        $appinfo_url    = $appinfo['url']

        $application_name_encode        = regsubst($application_name, '\s', '%20', 'G')
        $application_family_name_encode = regsubst($application_family_name, '\s', '%20', 'G')

        $server_name		= downcase($::fqdn)
        $onlyif_command       	= "if (`c:\appinfo\bin\curl.exe -s \"${appinfo_url}/api/application_families/${application_family_name_encode}/application_family_applications/${application_name_encode}.json?auth_token=${appinfo_token}\"` | select-string -quiet '(\"status\":404)') { exit 0 } else {exit 1}"
        $command              	= "c:\appinfo\bin\curl.exe -s -X POST --data \"auth_token=${appinfo_token}&application_family_application[application_id]=${application_name_encode}\" ${appinfo_url}/api/application_families/${application_family_name_encode}/application_family_applications.json"

        exec { "appinfo_register_application_family_${server_name}_${application_family_name}_${application_name}":
                command         => $command,
                onlyif          => $onlyif_command,
                provider        => powershell,
        }
}
