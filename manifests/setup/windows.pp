class appinfo::setup::windows {
	file { 'c:\\appinfo':
		ensure	=> 'directory',
	}

	file { 'c:\\appinfo\\bin':
		ensure	=> 'directory',
		recurse	=> true,
		purge	=> true,
		force	=> true,
		source	=> "puppet:///modules/appinfo/setup/windows",
	}
}
