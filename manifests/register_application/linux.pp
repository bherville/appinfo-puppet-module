define appinfo::register_application::linux ($application_name = $title, $application_type, $version_command, $major_application = true){
	$appinfo 	= hiera_hash('appinfo')
	$appinfo_token 	= $appinfo['token']
	$appinfo_url	= $appinfo['url']

	$application_name_encode	= regsubst($application_name, '\s', '%20', 'G')
	$version_command_encode		= uriescape($version_command)

	$server_name		= downcase($::fqdn)
	$onlyif_command 	= "test `curl -s '${appinfo_url}/api/applications/${application_name_encode}.json?auth_token=${appinfo_token}.json' | grep -oP '(\"status\":404)'`"
	$create_command 	= "curl -s --data \"auth_token=${appinfo_token}&application[name]=${application_name}&application[app_type]=${application_type}&application[version_command]=${version_command_encode}&application[major_application]=${major_application}\" ${appinfo_url}/api/applications.json"

	exec { "appinfo_create_application_${server_name}_${application_name}":
                command => $create_command,
                onlyif  => $onlyif_command,
                path    => ['/usr/bin','/bin'],
		require	=> Appinfo::Register_server[$::fqdn],
		notify  => Exec['update_hosted_applications'],
        }
}
