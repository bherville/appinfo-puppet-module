define appinfo::register_application::windows ($application_name = $title, $application_type, $version_command, $major_application = true){
	$appinfo 	= hiera_hash('appinfo')
	$appinfo_token 	= $appinfo['token']
	$appinfo_url	= $appinfo['url']

	$application_name_encode	= regsubst($application_name, '\s', '%20', 'G')

	$server_name	 	= downcase($::fqdn)
	$onlyif_command		= "if (`c:\appinfo\bin\curl.exe -s '${appinfo_url}/api/applications/${application_name_encode}.json?auth_token=${appinfo_token}.json'` | select-string -quiet '(\"status\":404)') { exit 0 } else {exit 1}"
	$create_command 	= "c:\appinfo\bin\curl.exe -s --data \"auth_token=${appinfo_token}&application[name]=${application_name}&application[app_type]=${application_type}&application[version_command]=${version_command}&application[major_application]=${major_application}\" ${appinfo_url}/api/applications.json"

        exec { "appinfo_create_application_${server_name}_${application_name}":
                command 	=> $create_command,
                onlyif  	=> $onlyif_command,
                require 	=> Appinfo::Register_server[$::fqdn],
                provider        => powershell,
        }
}
