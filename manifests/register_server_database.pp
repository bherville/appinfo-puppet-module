define appinfo::register_server_database ($database_name, $application_name) {
	case $::kernel {
		'Linux': {
			appinfo::register_server_database::linux { $title:
				database_name		=> $database_name,
				application_name	=> $application_name,
			}
		}
		'windows':  {
			appinfo::register_database_name::windows { $title:
                                database_name		=> $database_name,
				application_name	=> $application_name,
                        }
		}
	}
}
