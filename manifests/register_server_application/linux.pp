define appinfo::register_server_application::linux ($application_name = $title) {
	$appinfo 	= hiera_hash('appinfo')
	$appinfo_token 	= $appinfo['token']
	$appinfo_url	= $appinfo['url']

	$application_name_encode	= regsubst($application_name, '\s', '%20', 'G')

	$server_name		= downcase($::fqdn)
	$onlyif_command 	= "test `curl -s '${appinfo_url}/api/servers/${server_name}/applications/${application_name_encode}.json?auth_token=${appinfo_token}' | grep -oP '(\"status\":404)'`"
	$create_command		= "curl -s -X POST --data \"auth_token=${appinfo_token}&server_application[application_id]=${application_name_encode}\" ${appinfo_url}/api/servers/${server_name}/server_applications.json"

	exec { "appinfo_create_server_application_${server_name}_${application_name}":
		command	=> $create_command,
		onlyif	=> $onlyif_command,
                path    => ['/usr/bin','/bin'],
		notify	=> Exec['update_hosted_applications'],
        }
}
