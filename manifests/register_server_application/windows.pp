define appinfo::register_server_application::windows ($application_name = $title) {
	$appinfo 	= hiera_hash('appinfo')
	$appinfo_token 	= $appinfo['token']
	$appinfo_url	= $appinfo['url']

	$application_name_encode	= regsubst($application_name, '\s', '%20', 'G')

	$server_name                    = downcase($::fqdn)
        $onlyif_command       		= "if (`c:\appinfo\bin\curl.exe -s \"${appinfo_url}/api/servers/${server_name}/applications/${application_name_encode}.json?auth_token=${appinfo_token}\"` | select-string -quiet '(\"status\":404)') { exit 0 } else {exit 1}"
        $create_command              	= "c:\appinfo\bin\curl.exe -s -X POST --data \"auth_token=${appinfo_token}&server_application[application_id]=${application_name_encode}\" ${appinfo_url}/api/servers/${server_name}/server_applications.json"

        exec { "appinfo_create_server_application_${server_name}_${application_name}":
                command 	=> $create_command,
                onlyif  	=> $onlyif_command,
                provider        => powershell,
        }
}
