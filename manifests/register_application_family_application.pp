define appinfo::register_application_family_application ($application_family, $application_name = $title, $create_application_family = true) {
	case $::kernel {
		'Linux': {
			appinfo::register_application_family_application::linux { "${application_family}_${application_name}":
				application_family_name	=> $application_family,
				application_name	=> $application_name,
				require			=> Appinfo::Register_application_family[$application_family],
			}
		}
		'windows':  {
			appinfo::register_application_family_application::windows { "${application_family}_${application_name}":
                                application_family_name	=> $application_family,
				application_name	=> $application_name,
				require			=> Appinfo::Register_application_family[$application_family],
                        }
		}
	}

	if ($create_application_family) {
		if ! defined(Appinfo::Register_application_family[$application_family]) {
			appinfo::register_application_family { $application_family: }
		}
	}
}
