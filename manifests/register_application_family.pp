define appinfo::register_application_family ($application_family_name = $title) {
	case $::kernel {
		'Linux': {
			appinfo::register_application_family::linux { $application_family_name:
				application_family_name		=> $application_family_name,
			}
		}
		'windows':  {
			appinfo::register_application_family::windows { $application_family_name:
                                application_family_name        	=> $application_family_name,
                        }
		}
	}
}
