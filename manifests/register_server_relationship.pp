define appinfo::register_server_relationship ($server_primary = 'nil', $server_secondary, $port, $description = 'nil'){	
	case $::kernel {
		'Linux': {
			appinfo::register_server_relationship::linux { $title:
				server_primary		=> $server_primary,
				server_secondary	=> $server_secondary,
				port			=> $port,
				description		=> $description,
			}
		}
		'windows':  {
			appinfo::register_server_relationship::windows { $title:
				server_primary		=> $server_primary,
                                server_secondary        => $server_secondary,
                                port                    => $port,
                                description             => $description,
                        }
		}
	}
}
