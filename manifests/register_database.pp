define appinfo::register_database ($database, $dbms, $application){
	case $::kernel {
		'Linux': {
			appinfo::register_database::linux { $title:
				database		=> $database,
				dbms			=> $dbms,
				application		=> $application,
			}
		}
		'windows':  {
			appinfo::register_database::windows { $title:
                                database                => $database,
                                dbms                    => $dbms,
                                application             => $application,
                        }
		}
	}

	if ! defined(Appinfo::Register_server_database[$title]) {
		appinfo::register_server_database { $title:
			database_name  		=> $database,
			application_name	=> $application,
        	}
	}
}
