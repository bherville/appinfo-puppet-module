define appinfo::register_application_version::linux ($application_name = $title) {
	$appinfo 	= hiera_hash('appinfo')
	$appinfo_token 	= $appinfo['token']
	$appinfo_url	= $appinfo['url']

	$application_name_encode	= regsubst($application_name, '\s', '%20', 'G')

	$server_name	= downcase($::fqdn)
	$onlyif_command	= "test `curl -s '${appinfo_url}/api/servers/${server_name}/applications/${application_name_encode}/application_version.json?auth_token=${appinfo_token}' | grep -oP '(\"status\":404)'`"
	$create_command	= "curl -s -X POST --data \"auth_token=${appinfo_token}\" ${appinfo_url}/api/servers/${server_name}/applications/${application_name_encode}/application_version.json"

	exec { "appinfo_associate_application_${server_name}_${application_name}":
		command	=> $create_command,
		onlyif	=> $onlyif_command,
                path    => ['/usr/bin','/bin'],
		notify	=> Exec['update_hosted_applications'],
        }
}
