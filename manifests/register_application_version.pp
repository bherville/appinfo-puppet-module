define appinfo::register_application_version ($application_name = $title, $register_server_application = true) {
	case $::kernel {
		'Linux': {
			appinfo::register_application_version::linux { $application_name:
				application_name	=> $application_name,
			}
		}
		'windows':  {
			appinfo::register_application_version::windows { $application_name:
                                application_name        => $application_name,
                        }
		}
	}

	if ($register_server_application == true) {
                if ! defined(Appinfo::Register_server_application[$application_name]) {
                        appinfo::register_server_application { $application_name: 
				require	=> Appinfo::Register_application[$application_name],
				before	=> Appinfo::Register_application_version[$application_name],
			}
                }
        }
}
