define appinfo::register_server_database::linux ($database_name = $title, $application_name) {
	$appinfo 	= hiera_hash('appinfo')
	$appinfo_token 	= $appinfo['token']
	$appinfo_url	= $appinfo['url']

	$server_name		= downcase($::fqdn)
	$onlyif_command 	= "test `curl -s '${appinfo_url}/api/servers/${server_name}/server_databases/${database_name}.json?auth_token=${appinfo_token}' | grep -oP '(\"status\":404)'`"
	$create_command 	= "curl -s -X POST --data \"auth_token=${appinfo_token}&server_database[database_id]=${database_name}\" ${appinfo_url}/api/servers/${server_name}/server_databases.json"

	exec { "appinfo_register_server_database_${server_name}_${database_name}_${application_name}":
		command	=> $create_command,
		onlyif	=> $onlyif_command,
                path    => ['/usr/bin','/bin'],
		notify	=> Exec['update_hosted_applications'],
        }
}
