define appinfo::register_application_family::linux ($application_family_name = $title) {
	$appinfo 	= hiera_hash('appinfo')
	$appinfo_token 	= $appinfo['token']
	$appinfo_url	= $appinfo['url']

	$application_family_name_encode	= regsubst($application_family_name, '\s', '%20', 'G')

	$server_name		= downcase($::fqdn)
	$onlyif_command 	= "test `curl -s '${appinfo_url}/api/application_families/${application_family_name_encode}?auth_token=${appinfo_token}.json' | grep -oP '(\"status\":404)'`"
	$create_command 	= "curl -s -X POST --data \"auth_token=${appinfo_token}&application_family[name]=${application_family_name_encode}\" ${appinfo_url}/api/application_families.json"

	exec { "appinfo_register_application_family_${server_name}_${application_family_name}":
		command	=> $create_command,
		onlyif	=> $onlyif_command,
                path    => ['/usr/bin','/bin'],
		notify	=> Exec['update_hosted_applications'],
        }
}
